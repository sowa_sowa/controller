<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitab3aae83d9a387a485399d83aa900240
{
    public static $files = array (
        '5a53be654e4f73848e0e2e995493897f' => __DIR__ . '/../..' . '/app/config.php',
        'c1d952ed5c3af6ebf32482823b87babf' => __DIR__ . '/../..' . '/app/routes.php',
    );

    public static $prefixLengthsPsr4 = array (
        'F' => 
        array (
            'Framework\\' => 10,
        ),
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Framework\\' => 
        array (
            0 => __DIR__ . '/../..' . '/framework',
        ),
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/app',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitab3aae83d9a387a485399d83aa900240::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitab3aae83d9a387a485399d83aa900240::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
